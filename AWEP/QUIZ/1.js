
function validateform() {
    const username = document.querySelector('#d1_u').value;
    const password = document.querySelector('#d1_p').value;
    const username1 = document.querySelector('#d1');
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;


    if (username === null || username === "") {
        alert("Name can't be blank");
        return false;
    }
    else if (password === null || password === "") {
        alert("Password can't be blank");
        return false;
    }
    else if (password.length <= 5 || password >= 12) {
        if (password.length <= 5) {
            alert("Password must be at least 5 characters long.");
            return false;
        }
        if (password.length >= 12) {
            alert("Password must be at max 12 characters long.");
            return false;
        }

    }
}
